package crud;

import model.Musei;
import model.Opere;

import java.sql.*;

public class MuseiCRUD {

    private Connection connection;

    public MuseiCRUD(Connection connection){
        this.connection = connection;
    }

    public void insert(Musei musei){
        String query = "INSERT INTO musei" +
                "VALUES(?, ?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, musei.getNomeM());
            statement.setString(2, musei.getCitta());

            statement.executeUpdate();
            System.out.println("Inserito museo.");
        } catch (SQLException e) {
            System.out.println("Inserimento fallito.");
            e.printStackTrace();
        } finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Musei selectByNome(String nomeMuseo){

        String query = "select m.nome_m, m.citta " +
                "from musei m " +
                "where m.nome_m = ?";

        PreparedStatement statement = null;
        ResultSet set = null;
        Musei musei = null;
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, nomeMuseo);

            set = statement.executeQuery();
            if (set.next()){
                musei = new Musei();
                musei.setNomeM(nomeMuseo);
                musei.setCitta(set.getString(2));

                return musei;
            }
        } catch (SQLException e) {
            System.out.println("Non esistono musei col nome: " + nomeMuseo);
            return null;
        } finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;

    }

    /*public Musei selectAll(){
        Statement statement = null;
        String query = "SELECT NOME_M, CITTA " +
                "FROM MUSEI";
        try {
            statement = connection.createStatement();
            ResultSet set = statement.executeQuery(query);
            while (set.next()){
                String nomeM = set.getString("NOME_M");
                String citta = set.getString("CITTA");

                System.out.println("Nome museo: " + nomeM + "\nCitta: " + citta);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }*/

}



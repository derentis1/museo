package crud;

import model.Opere;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OpereCRUD {

    private Connection connection;

    public OpereCRUD(Connection connection){
        this.connection = connection;
    }

    public Opere selectById(long idOpera){
        String query = "SELECT o.codice, o.titolo, o.nome_m, o.nome_a " +
                "FROM opere o " +
                "WHERE o.codice = ?";

        PreparedStatement statement = null;
        ResultSet set = null;
        Opere opere = null;
        try {
            statement = connection.prepareStatement(query);
            statement.setLong(1, idOpera);

            set = statement.executeQuery();
            if (set.next()){
                opere = new Opere();
                opere.setCodice(idOpera);
                opere.setTitolo(set.getString(2));
                opere.setNomeM(set.getString(3));
                opere.setNomeA(set.getString(4));

                return opere;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return opere;
    }

    public Opere selectByArtistAndMuseum(String nomeMuseum, String nomeArtist){

        String query = "SELECT o.codice, o.titolo " +
                       "FROM opere o " +
                       "WHERE o.nome_m = ? " +
                       "AND o.nome_a = ?";

        PreparedStatement statement = null;
        ResultSet set = null;
        Opere opere = null;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, nomeMuseum);
            statement.setString(2, nomeArtist);

            set = statement.executeQuery();
            if (set.next()){
                opere = new Opere();

                opere.setCodice(set.getLong(1));
                opere.setTitolo(set.getString(2));
                opere.setNomeM(nomeMuseum);
                opere.setNomeA(nomeArtist);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return opere;
    }

}

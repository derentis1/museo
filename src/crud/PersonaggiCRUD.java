package crud;

import model.Personaggi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonaggiCRUD {

    private Connection connection;

    public PersonaggiCRUD(Connection connection){
        this.connection = connection;
    }

    public Personaggi selectByNome(String nomePersonaggio){
        String query = "SELECT p.personaggio, p.codice " +
                "FROM personaggi p " +
                "where p.personaggio = ?";

        PreparedStatement statement = null;
        ResultSet set;
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, nomePersonaggio);

            set = statement.executeQuery();
            if (set.next()){
                Personaggi personaggi = new Personaggi();
                personaggi.setPersonaggio(nomePersonaggio);
                personaggi.setCodice(set.getLong(2));

                return personaggi;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

}

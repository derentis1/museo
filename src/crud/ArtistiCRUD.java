package crud;

import model.Artisti;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistiCRUD {

    private Connection connection;

    public ArtistiCRUD(Connection connection){
        this.connection = connection;
    }

    public Artisti selectByNome(String nomeArtista){
        String query = "select a.nome_a, a.nazionalita " +
                "from artisti a " +
                "where a.nome_a = ?";

        PreparedStatement statement = null;
        ResultSet set = null;
        Artisti artisti = null;
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, nomeArtista);

            set = statement.executeQuery();
            if (set.next()){
                artisti = new Artisti();
                artisti.setNomeA(nomeArtista);
                artisti.setNazionalita(set.getString(2));

                return artisti;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return artisti;
    }

    public Artisti selectAll(){
        String query = "select * from artisti";

        PreparedStatement statement = null;
        ResultSet set = null;
        try {
            statement = connection.prepareStatement(query);


            set = statement.executeQuery();
            if (set.next()){
                Artisti artisti = new Artisti();
                artisti.setNomeA(set.getString("nome_a"));
                artisti.setNazionalita(set.getString("nazionalita"));

                return artisti;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

}

package main;

import java.sql.*;

public class JDBConn {

    private static final String URL = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
    private static final String USER = "museo";
    private static final String PASSWORD = "password";

    public static void main(String[] args) {

        Connection connection = null;
        try {

            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement statement = null;
            /*System.out.println("Il codice ed il titolo delle opere di Tiziano conservate alla “National Gallery”.\n");
            String query1 = "SELECT o.codice, o.titolo " +
                    "FROM opere o " +
                    "WHERE o.nome_a = 'Tiziano' " +
                    "AND o.nome_m = 'National Gallery'";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query1);
                while (set.next()) {

                    long codice = set.getLong(1);
                    String titolo = set.getString(2);

                    System.out.println("Codice: " + codice + "\nTitolo: " + titolo);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            System.out.println("\nIl nome dell’artista ed il titolo delle opere " +
                    "conservate alla “Galleria degli Uffizi” o alla\n" +
                    "“National Gallery”.");
            System.out.println();
            String query2 = "SELECT o.nome_a, o.titolo " +
                    "FROM opere o " +
                    "WHERE o.nome_m = 'National Gallery' " +
                    "OR o.nome_m = 'Galleria degli Uffizi' ";

            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query2);
                while (set.next()) {
                    String nomeA = set.getString(1);
                    String titolo = set.getString(2);

                    System.out.println("Artist name: " + nomeA + "\nTitle: " + titolo);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            System.out.println("Il nome dell’artista ed il titolo delle opere conservate nei musei di Firenze");
            String query3 = "SELECT o.nome_a, o.titolo " +
                    "FROM opere o, musei m " +
                    "WHERE o.nome_m = m.nome_m " +
                    "AND m.citta = 'Firenze' ";

            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query3);
                while (set.next()){
                    String nomeA = set.getString(1);
                    String titolo = set.getString(2);

                    System.out.println("Artist name: " + nomeA + "\nTitle: " + titolo);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }*/

            /*System.out.println("Le città in cui sono conservate opere di Caravaggio");
            String query4 = "SELECT m.citta " +
                    "FROM opere o, artisti a, musei m " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND a.nome_a = o.nome_a " +
                    "AND a.nome_a = 'Caravaggio'";

            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query4);
                while (set.next()){
                    String citta = set.getString(1);

                    System.out.println("Citta: " + citta);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } */

            /*System.out.println("Il codice ed il titolo delle opere di Tiziano conservate nei musei di Londra\n");
            String query5 = "SELECT o.codice, o.titolo " +
                    "FROM opere o, musei m " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND o.nome_a = 'Tiziano' " +
                    "AND m.citta = 'London'";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query5);
                while (set.next()){
                    long codice = set.getLong(1);
                    String titolo = set.getString(2);

                    System.out.println("Codice: " + codice + "\nTitolo: " + titolo);
                }
            }*/

            /*System.out.println("Il nome dell’artista ed il titolo delle opere di " +
                    "artisti spagnoli conservate nei musei di Firenze\n");
            String query6 = "SELECT a.nome_a, o.titolo " +
                    "FROM opere o, artisti a, musei m " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND a.nome_a = o.nome_a " +
                    "AND a.nazionalita = 'Italian' " +
                    "AND m.citta = 'Firenze'";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query6);

                while (set.next()){
                    String nomeA = set.getString(1);
                    String titolo = set.getString(2);

                    System.out.println("Artist name: " + nomeA + "\nTitolo: " + titolo);
                }
            }*/

            /*System.out.println("Il codice ed il titolo delle opere di " +
                    "artisti italiani conservate nei musei di Londra, in cui è " +
                    "rappresentata la Madonna");
            String query7 = "SELECT o.codice, o.titolo " +
                    "FROM musei m, artisti a, opere o, personaggi p " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND a.nome_a = o.nome_a " +
                    "AND o.codice = p.codice " +
                    "AND a.nazionalita = 'Italian' " +
                    "AND m.citta = 'London' " +
                    "AND p.personaggio = 'Madonna'";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query7);
                while (set.next()){
                    long codice = set.getLong(1);
                    String titolo = set.getString(2);

                    System.out.println("Codice: " + codice + "\nTitolo: " + titolo);
                }
            }*/

            /*System.out.println("Per ciascun museo di Londra, il numero di " +
                    "opere di artisti italiani ivi conservate");

            String query8 = "SELECT count(*), m.citta " +
                    "FROM musei m, artisti a, opere o " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND a.nome_a = o.nome_a " +
                    "AND a.nazionalita = 'Italian' " +
                    "AND m.citta = 'London' " +
                    "GROUP BY m.citta";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query8);
                while (set.next()){
                    int count = set.getInt(1);
                    String citta = set.getString(2);

                    System.out.println("Count: " + count + "\nCitta: " + citta);
                }
            }*/

            /*System.out.println("Il nome dei musei di Londra che non conservano opere di Tiziano");
            String query9 = "SELECT m.nome_m " +
                    "FROM opere o, musei m " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND m.citta = 'London' " +
                    "AND NOT EXISTS ( " +
                    "SELECT a.nome_a " +
                    "FROM artisti a " +
                    "WHERE a.nome_a = o.nome_a " +
                    "AND a.nome_a = 'Tiziano' " +
                    ")";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query9);
                while (set.next()){
                    String nomeM = set.getString(1);

                    System.out.println("Museum name: " + nomeM);
                }
            }*/

            /*System.out.println("Il nome dei musei di Londra che conservano solo opere di Tiziano");
            String query10 = "SELECT distinct m.nome_m " +
                    "FROM opere o, musei m " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND m.citta = 'London' " +
                    "AND NOT EXISTS ( " +
                    "SELECT a.nome_a " +
                    "FROM artisti a " +
                    "WHERE a.nome_a = o.nome_a " +
                    "AND a.nome_a != 'Tiziano' " +
                    ")";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query10);
                while (set.next()){
                    String nomeM = set.getString(1);

                    System.out.println("Museum name: " + nomeM);
                }
            }*/

            /*System.out.println("Per ciascun artista, il nome dell’artista ed il " +
                    "numero di sue opere conservate alla “Galleria " +
                    "degli Uffizi”");
            String query11 = "SELECT a.nome_a, count(*) " +
                    "FROM opere o, artisti a " +
                    "WHERE a.nome_a = o.nome_a " +
                    "AND o.nome_m = 'Galleria degli Uffizi' " +
                    "GROUP BY a.nome_a";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query11);
                while (set.next()){
                    String nomeA = set.getString(1);
                    int count = set.getInt(2);

                    System.out.println("Artist name: " + nomeA + "\nCount: " + count);
                }
            }*/

            /*System.out.println("I musei che conservano almeno 20 opere di artisti italiani");
            String query12 = "SELECT m.nome_m, count(*) " +
                    "FROM opere o, musei m, artisti a " +
                    "WHERE m.nome_m = o.nome_m " +
                    "AND a.nome_a = o.nome_a " +
                    "AND a.nazionalita = 'Italian' " +
                    "HAVING count(a.nazionalita) > 1 " +
                    "GROUP BY m.nome_m";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query12);
                while (set.next()){
                    String nomeM = set.getString(1);
                    int count = set.getInt(2);

                    System.out.println("Museum name: " + nomeM + "\nCount: " + count);
                }
            }*/

            /*System.out.println("Per le opere di artisti italiani che " +
                    "non hanno personaggi, il titolo dell’opera ed il nome " +
                    "dell’artista");
            String query13 = "SELECT o.titolo, o.nome_a " +
                    "FROM opere o, artisti a " +
                    "WHERE a.nome_a = o.nome_a " +
                    "AND a.nazionalita = 'Italian' " +
                    "AND NOT EXISTS ( " +
                    "SELECT p.personaggio " +
                    "FROM personaggi p " +
                    "WHERE o.codice = p.codice " +
                    ")";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query13);
                while (set.next()){
                    String titolo = set.getString(1);
                    String nomeA = set.getString(2);

                    System.out.println("Titolo: " + titolo + "\nNome A: " + nomeA);
                }
            }*/

            /*System.out.println("Il nome dei musei di Londra che non conservano " +
                    "opere di artisti italiani, eccettuato Tiziano");
            String query14 = "SELECT m.nome_m " +
                    "FROM musei m, opere o " +
                    "WHERE o.nome_m = m.nome_m " +
                    "AND m.citta = 'London' " +
                    "AND NOT EXISTS ( " +
                    "SELECT a.nazionalita " +
                    "FROM artisti a " +
                    "WHERE o.nome_a = a.nome_a " +
                    "AND a.nazionalita = 'Italian' " +
                    "AND a.nome_a != 'Tiziano' " +
                    ")";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query14);
                while (set.next()){
                    String nomeM = set.getString(1);

                    System.out.println("Museum name: " + nomeM);
                }
            }*/

            System.out.println("Per ogni museo, il numero di opere divise " +
                    "per la nazionalità dell’artista");
            String query15 = "SELECT count(*), o.nome_m " +
                    "FROM opere o, artisti a " +
                    "WHERE a.nome_a = o.nome_a " +
                    "AND a.nazionalita = 'Italian' " +
                    "GROUP BY o.nome_m";
            try {
                statement = connection.createStatement();
                ResultSet set = statement.executeQuery(query15);
                while (set.next()){
                    int count = set.getInt(1);
                    String nomeM = set.getString(2);

                    System.out.println("Count: " + count + "\nMuseum name: " + nomeM);
                }
            }

            catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    statement.close();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}

package main;

import connection.ConnectionDatabase;
import crud.ArtistiCRUD;
import crud.MuseiCRUD;
import crud.OpereCRUD;
import crud.PersonaggiCRUD;
import model.Artisti;
import model.Musei;
import model.Opere;
import model.Personaggi;

import java.sql.Connection;
import java.sql.SQLException;

public class QueryMain {

    public static void main(String[] args) {

        Connection connection = ConnectionDatabase.getConnection();

        /*if (connection != null){
            System.out.println("Connessione al DB Musei effettuata.\n");
            MuseiCRUD museiCRUD = new MuseiCRUD(connection);
            Musei m1 = museiCRUD.selectByNome("National Gallery");

            if (m1 != null){
                System.out.println("Nome museo: " + m1.getNomeM());
                System.out.println("Citta: " + m1.getCitta());
            }
        }*/

        /*f (connection != null){
            System.out.println("Connessione al DB Musei effettuata.\n");
            MuseiCRUD museiCRUD = new MuseiCRUD(connection);
            Musei m = museiCRUD.selectByNome("Museo del Novecento");
            Musei m1 = museiCRUD.selectByNome("National Gallery");

            if (m != null){
                System.out.println("Nome museo: " + m.getNomeM());
                System.out.println("Citta: " + m.getCitta());
            }

            if (m1 != null){
                System.out.println("Nome museo: " + m1.getNomeM());
                System.out.println("Citta: " + m1.getCitta());
            }
        }
        System.out.println("");*/

        /*if(connection != null){
            MuseiCRUD museiCRUD = new MuseiCRUD(connection);
            Musei m = museiCRUD.selectAll();

        }*/

        /*if (connection != null){
            System.out.println("Connessione al DB Artisti effettuata.\n");
            ArtistiCRUD artistiCRUD = new ArtistiCRUD(connection);
            Artisti a1 = artistiCRUD.selectAll();

            if (a1 != null){
                System.out.println("Nome artista: " + a1.getNomeA());
                System.out.println("Nazionalita: " + a1.getNazionalita());
            }
        }
        System.out.println("");*/


        if (connection != null){
            System.out.println("Connessione al DB Opere effettuata.\n");
            OpereCRUD opereCRUD = new OpereCRUD(connection);
            Opere o1 = opereCRUD.selectById(1);
            if (o1 != null){
                System.out.println("Codice: " + o1.getCodice());
                System.out.println("Titolo: " + o1.getTitolo());
                System.out.println("Nome Museo: " + o1.getNomeM());
                System.out.println("Nome Artista: " + o1.getNomeA());
            }
        }
        System.out.println("");

        /*if (connection != null){
            System.out.println("Connessione al DB Personaggi effettuata.\n");
            PersonaggiCRUD personaggiCRUD = new PersonaggiCRUD(connection);
            Personaggi p1 = personaggiCRUD.selectByNome("Fiammetta Bianchini");
            if (p1 != null){
                System.out.println("Nome personaggio: " + p1.getPersonaggio());
                System.out.println("Codice: " + p1.getCodice());
            }
        }
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }*/


        if (connection != null){
            System.out.println("Connessione al DB Opere effettuata.\n");
            OpereCRUD opereCRUD = new OpereCRUD(connection);
            Opere o = opereCRUD.selectByArtistAndMuseum("Tiziano", "National Gallery");
            if (o != null){
                System.out.println("Codice: " + o.getCodice());
                System.out.println("Titolo: " + o.getTitolo());
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}

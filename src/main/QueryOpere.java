package main;

import connection.ConnectionDatabase;
import crud.OpereCRUD;
import model.Opere;

import java.sql.Connection;
import java.sql.SQLException;

public class QueryOpere {

    public static void main(String[] args) {

        Connection connection = ConnectionDatabase.getConnection();

        OpereCRUD opereCRUD = new OpereCRUD(connection);
        if (connection != null) {
            System.out.println("Connessione al DB Opere effettuata");
            Opere o = opereCRUD.selectById(1);
            if (o != null) {

                System.out.println("Codice: " + o.getCodice());
                System.out.println("Titolo: " + o.getTitolo());
                System.out.println("Nome Artist: " + o.getNomeA());
                System.out.println("Nome Museum: " + o.getNomeM());

            }
        }
        System.out.println();

        if (connection != null) {
            System.out.println("Connessione al DB Opere effettuata");

            Opere o = opereCRUD.selectByArtistAndMuseum("National Gallery", "Tiziano");

            if (o != null) {

                System.out.println("Codice: " + o.getCodice());
                System.out.println("Titolo: " + o.getTitolo());

            }

        }
    }
}


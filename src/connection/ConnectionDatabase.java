package connection;

import oracle.jdbc.OracleDriver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDatabase {

    private static final String URL = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
    private static final String USER = "museo";
    private static final String PASSWORD = "password";

    public static Connection getConnection(){

        try {
            Driver driver = new OracleDriver();
            DriverManager.registerDriver(driver);
            return DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Impossibile creare la connessione");
        }

        return null;
    }

}
